import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/web_app/new.php')

WebUI.setText(findTestObject('MainPage/input_Employee Name_name'), name)

WebUI.setText(findTestObject('MainPage/input_Password_password'), password)

WebUI.setText(findTestObject('MainPage/input_Phone Number_phone_number'), phone)

WebUI.setText(findTestObject('MainPage/input_Email_email'), email)

if (type == 'Fulltime') {
    WebUI.selectOptionByValue(findTestObject('MainPage/select_type'), '1', false)
} else {
    WebUI.selectOptionByValue(findTestObject('MainPage/select_type'), '2', false)

}

WebUI.click(findTestObject('MainPage/create'))

WebUI.verifyTextPresent('Missing Information', false)

