# Tests
Test Plan

Objective : To ensure that user can make use of the website to self register.

Test Coverage
1.	Verify that all the required fields are available
	a.	Employee Name
	b.	Phone Number
	c.	Password
	d.	Email 
	e.	Employee Type
	f.	Create button
2.	Verify that Employee details is created in the DB after user clicks on Create and Screen displayed with Dashboard
3.	Verify if email is sent out to the employee after creation
4.	Verify error displayed if there are missing information when employee tries to insert
5.	Verify that there are validation on 
	a.	Email address format
	b.	Password format
6.	Verify if there are warning displayed when employee tries to register more than once


Automation Test :

As the automation test is written in Katalon Studio. You will need to install Katalon Studio which can be downloaded from https://www.katalon.com/katalon-studio/

Basically the framework is as follows:
Object Repository -  where all the elements are located
Test Cases - where all the testcases are located
Data files - Test data - all the valid and invalid testdata
Test Suite - where the testsuites are located

Please refer to Automation Test Details.docx for more details

The language used = Groovy

To execute the automation test:
1. Open Katalon Studio
2. Open the Project file - TestProject.prj where you had have place the test folder
3. Open Test Suite - SelfRegisterPortal and click on the execute button 
 


 - How you _would_ have liked to write the tests.
 
I would like the tests to be Data Driven instead of having 1 testcase for each scenario.
Have 1 testcase that was able to cover all the different scenarios


- What challenges you faced when deciding on a solution.
How much to cover. Whether to cover all the invalid scenarios like all the missing information on each field, 
missing information on all the fields etc. 

- Any additional information you would like to know to do a better job.
1) What type of validation are they expecting like checking on the email address entered etc
2) What is the maximum number of characters required for each field
3) what password format etc
	 
