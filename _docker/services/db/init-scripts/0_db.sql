CREATE DATABASE IF NOT EXISTS `skilltestertest`;
GRANT ALL ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'root'@'localhost';

GRANT ALL ON *.* TO 'skilltester'@'%';

USE skilltestertest;
